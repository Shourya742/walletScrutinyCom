---
wsId: iconomi
title: 'ICONOMI: Buy and Sell Crypto'
altTitle: 
authors:
- danny
appId: net.iconomi.iconomi
appCountry: si
idd: 1238213050
released: 2017-05-25
updated: 2023-10-06
version: 3.1.1
stars: 4.6
reviews: 82
size: '68562944'
website: http://www.iconomi.com
repository: 
issue: 
icon: net.iconomi.iconomi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: iconomicom
social:
- https://www.linkedin.com/company/iconominet
- https://www.facebook.com/iconomicom
- https://www.reddit.com/r/ICONOMI
features: 
developerName: ICONOMI AG

---

{% include copyFromAndroid.html %}

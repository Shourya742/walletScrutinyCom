---
wsId: safemoon
title: SafeMoon
altTitle: 
authors:
- danny
appId: net.safemoon.ioswallet
appCountry: us
idd: '1579735495'
released: 2021-10-06
updated: 2023-09-08
version: '3.49'
stars: 4.8
reviews: 14611
size: '142267392'
website: https://safemoon.com/
repository: 
issue: 
icon: net.safemoon.ioswallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-23
signer: 
reviewArchive: 
twitter: safemoon
social: 
features: 
developerName: Safemoon US LLC

---

{% include copyFromAndroid.html %}
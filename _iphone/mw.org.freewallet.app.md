---
wsId: mwfreewallet
title: Freewallet Multi Crypto Wallet
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
appCountry: 
idd: 1274003898
released: 2017-09-01
updated: 2023-10-10
version: 1.18.6
stars: 3.8
reviews: 1311
size: '71025664'
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-10-10
signer: 
reviewArchive: 
twitter: freewalletorg
social:
- https://www.facebook.com/freewallet.org
features: 
developerName: Freewallet

---

{% include copyFromAndroid.html %}

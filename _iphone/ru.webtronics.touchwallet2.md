---
wsId: touchWalletCrypto
title: Touch Wallet — Crypto Wallet
altTitle: 
authors:
- danny
appId: ru.webtronics.touchwallet2
appCountry: us
idd: '6443906980'
released: 2022-11-01
updated: 2023-09-27
version: 1.6.13
stars: 4.3
reviews: 6
size: '65183744'
website: https://touchwallet.com/
repository: 
issue: 
icon: ru.webtronics.touchwallet2.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-02
signer: 
reviewArchive: 
twitter: Touch_Wallet
social:
- https://t.me/TouchWalletEN
features: 
developerName: CLOUD FARMER L.L.C-FZ

---

{% include copyFromAndroid.html %}

---
wsId: tuxWallet
title: TUX WALLET
altTitle: 
authors:
- danny
appId: net.tuxglobal.wallet
appCountry: kw
idd: '1495945761'
released: 2020-02-04
updated: 2023-10-17
version: 1.7.7
stars: 0
reviews: 0
size: '72124416'
website: https://tux-wallet.com/
repository: 
issue: 
icon: net.tuxglobal.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-18
signer: 
reviewArchive: 
twitter: Coinyexdotcom
social:
- https://t.me/coinyexchannel
features: 
developerName: Coinyex Co., Ltd.

---

{% include copyFromAndroid.html %}

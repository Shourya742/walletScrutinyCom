---
wsId: stablexKripto
title: Stablex | Kripto Para Al-Sat
altTitle: 
authors:
- danny
appId: net.stablex.app
appCountry: tr
idd: '1591457284'
released: 2021-10-26
updated: 2023-10-10
version: 2.3.23
stars: 4.4
reviews: 25
size: '53775360'
website: https://stablex.com.tr
repository: 
issue: 
icon: net.stablex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: Stablex_Turkey
social:
- https://www.instagram.com/stablexofficial
- https://www.youtube.com/channel/UC261DPjgdgZcP9PgoKrGc7A
- https://www.linkedin.com/company/stablex-net
features: 
developerName: STABLEX BILISIM TEKNOLOJI ANONIM SIRKETI

---

{% include copyFromAndroid.html %}

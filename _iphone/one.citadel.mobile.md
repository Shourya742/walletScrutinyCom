---
wsId: CitadelOne
title: Citadel One
altTitle: 
authors:
- danny
appId: one.citadel.mobile
appCountry: us
idd: '1546701475'
released: 2021-04-06
updated: 2023-05-19
version: 2.9.5
stars: 4.2
reviews: 10
size: '97445888'
website: https://citadel.one/
repository: https://github.com/citadeldao
issue: 
icon: one.citadel.mobile.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-12-08
signer: 
reviewArchive: 
twitter: CitadelDAO
social:
- https://www.reddit.com/r/citadeldao/
- https://medium.com/citadel-one
- https://www.instagram.com/citadeldao/
- https://www.youtube.com/channel/UCl6VvVIrAr7ztJ1Wf7xBxrA
- https://www.linkedin.com/company/citadel-one/
- https://www.facebook.com/Citadel.One
features: 
developerName: Citadel.one LTD

---

{% include copyFromAndroid.html %}
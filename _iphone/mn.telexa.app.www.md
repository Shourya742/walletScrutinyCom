---
wsId: mnTelexa
title: Telexa.mn - Хөрөнгө оруулалт
altTitle: 
authors:
- danny
appId: mn.telexa.app.www
appCountry: mn
idd: '1596968900'
released: 2021-12-02
updated: 2023-10-16
version: '7.2'
stars: 4.8
reviews: 2797
size: '55856128'
website: https://www.telexa.mn/
repository: 
issue: 
icon: mn.telexa.app.www.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/Telexa.mn/
features: 
developerName: Save Inc.

---

{% include copyFromAndroid.html %}

---
wsId: LMAXGlobal
title: LMAX Global Trading
altTitle: 
authors:
- danny
appId: mobiletradingpartners.lmax.exchange.iphone
appCountry: hu
idd: 884042608
released: 2014-06-06
updated: 2023-09-28
version: 4.4.323
stars: 0
reviews: 0
size: '122797056'
website: https://www.lmax.com/mobile
repository: 
issue: 
icon: mobiletradingpartners.lmax.exchange.iphone.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-11-10
signer: 
reviewArchive: 
twitter: LMAX
social:
- https://www.linkedin.com/company/lmax-group
features: 
developerName: LMAX BROKER LIMITED

---

{% include copyFromAndroid.html %}

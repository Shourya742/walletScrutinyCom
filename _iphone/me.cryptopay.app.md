---
wsId: cryptopay
title: 'Cryptopay: Buy Bitcoin Safely'
altTitle: 
authors: 
appId: me.cryptopay.app
appCountry: de
idd: 1223340174
released: 2017-06-08
updated: 2023-10-19
version: 1.64.2
stars: 4.5
reviews: 313
size: '170280960'
website: https://cryptopay.me/
repository: 
issue: 
icon: me.cryptopay.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-03-10
signer: 
reviewArchive: 
twitter: cryptopay
social:
- https://www.linkedin.com/company/cryptopay
- https://www.facebook.com/cryptopayme
features: 
developerName: Cryptopay

---

In the description the only sentence hinting at custodianship is:

> Use our secure multisig wallet to receive, store and transfer BTC, LTC, XRP,
  ETH to your friends.

but there is nothing more to be found and as "multisig wallet" could refer to
anything, we can't say with certainty that this wallet even tries to imply
being self-custodial and therefore consider it **not verifiable**.

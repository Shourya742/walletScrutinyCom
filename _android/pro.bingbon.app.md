---
wsId: bingbon
title: BingX Trade BTC, Buy Crypto
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: pro.bingbon.app
appCountry: us
released: 2019-05-18
updated: 2023-10-10
version: 4.2.4
stars: 4.4
ratings: 2828
reviews: 591
size: 
website: https://bingx.com
repository: 
issue: 
icon: pro.bingbon.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-21
signer: 
reviewArchive: 
twitter: BingbonOfficial
social:
- https://www.linkedin.com/company/bingbon
- https://www.facebook.com/BingbonOfficial
- https://www.reddit.com/r/Bingbon
redirect_from: 
developerName: BingX
features: 

---

We cannot find any claims as to the custody of private keys found from Bingbon.
We must assume the wallet app is custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

---
wsId: eliteWallet
title: Elite Wallet
altTitle: 
authors:
- danny
users: 1000
appId: sc.elitewallet.elitewallet
appCountry: 
released: 2022-11-22
updated: 2023-06-02
version: 1.1.7
stars: 4.5
ratings: 
reviews: 12
size: 
website: https://elitewallet.sc
repository: https://github.com/Elite-Labs/EliteWallet/releases/
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/497
icon: sc.elitewallet.elitewallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-08-24
signer: 
reviewArchive: 
twitter: EliteWallet
social:
- https://t.me/elite_wallet
- https://www.reddit.com/user/EliteTechnologies
redirect_from: 
developerName: Elite Lab
features: 

---

## App Description from Google Play

> Elite Wallet respects your privacy and allows you to safely store, exchange, and spend your Monero, Bitcoin, Litecoin, and Haven. Elite Wallet is focused on high privacy standards.
>
> Features of Elite Wallet:
>
> - Completely noncustodial and open-source. Your keys, your coins
> - Easily exchange between BTC, LTC, XMR, and dozens of other cryptocurrencies
> - Buy Bitcoin/Litecoin with credit/debit/bank and sell Bitcoin by bank transfer
> - Create multiple Bitcoin, Litecoin, Monero, and Haven wallets
> - You control your own seed and keys, including your Monero private view key

## Analysis 

- The app's homepage is currently offline, but is available via [archive.org](https://web.archive.org/web/20230305015316/https://elitewallet.sc/)
- We successfully installed the app, and it describes itself as:
  > *Awesome wallet for Monero, bitcoin, Litecoin, and Haven*
  > - With standard anonymity, no proxy server is used. 
  > - With advanced anonymity, Elite proxy server is used to anonymize internet traffic
  > - Elite anonymity uses a custom proxy server to anonymize internet traffic
- We tested standard anonymity and created a wallet.
- We assigned a 4-digit pin.
- We chose Bitcoin (Electrum) as the wallet currency and assigned 'test' as the wallet name.
- The 24-word seed phrases were provided.
- We were provided with a Bech32 BTC address
- The seed phrases can be backed-up via the security and backup settings.

This app is **[for verification](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/497)**.